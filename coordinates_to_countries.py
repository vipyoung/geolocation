# Author: Sofiane
"""
Read from the standard input pairs of coordinates lat\tlng then map them to countries. 
The accuracy of this script is to be verified against Nominatim for example. 
"""

import countries
import sys, ast

cc = countries.CountryChecker('TM_WORLD_BORDERS/TM_WORLD_BORDERS-0.3.shp')
for line in sys.stdin:
	latitude, longitude = line.strip().split('\t')
	latitude = float(latitude)
	longitude = float(longitude)
	try:
		country = cc.getCountry(countries.Point(latitude, longitude))
	except:
		country = 'None'
	print country
		

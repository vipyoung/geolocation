# README #
This code resolve geo coordinates into countries using shape files. 
To run the script, prepare the input file in the format latitude[tabulation]longitude, e.g.: 
lat\tlng
lat\tlng

Then run:
cat input.tsv | python coordinates_to_countries.py

More will come later.